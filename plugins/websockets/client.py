#!/usr/bin/env python
# WS client example from https://websockets.readthedocs.io/en/stable/intro.html#common-patterns
# Can also connect on an interctive terminal with python -m websockets ws://localhost:3000

import asyncio
import websockets


async def hello():
    uri = "ws://localhost:3000"
    async with websockets.connect(uri) as websocket:
        name = input("What's your name? ")

        await websocket.send(name)
        print(f"> {name}")

        greeting = await websocket.recv()
        print(f"< {greeting}")


if __name__ == "__main__":
    asyncio.get_event_loop().run_until_complete(hello())
