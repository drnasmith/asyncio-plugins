import websockets


class WebSocketServer:
    def __init__(self, host="localhost", port=3000):
        self.port = port
        self.host = host
        self.connections = set()

    def register_connection(self, connection):
        print(f"New Connection Received")
        self.connections.add(connection)

    def unregister_connection(self, connection):
        self.connections.remove(connection)
        print(f"Connection Closed")

    async def process_message(self, message, websocket):
        print(f"Received message: {message}")
        tokens = message.split(" ")
        if tokens[0] == "HELP":
            reply = f"Valid commands are JOIN <user>, MSG <message string>"
        elif tokens[0] == "JOIN":
            reply = f"New User joined: {tokens[1]}"
        elif tokens[0] == "MSG":
            reply = f"Message {' '.join(tokens[1:])} sent"
        else:
            reply = f"Message {tokens[0]} not supported"
        await self.send(reply)

    async def handle(self, websocket, path):
        self.register_connection(websocket)
        try:
            # await self.send(websocket, "HANDSHAKE EVENT")
            async for message in websocket:
                await self.process_message(message, websocket)
        finally:
            self.unregister_connection(websocket)

    async def send(self, message):
        for client in self.connections:
            await client.send(message)
            print(f">Sent greeting for: {message}")

    def listen(self):
        print(f"Starting websockets on {self.host}:{self.port}")
        return websockets.serve(self.handle, self.host, self.port)


def websocket_task():
    ws = WebSocketServer("localhost", 8080)
    return ws.listen()


if __name__ == "__main__":
    import asyncio

    ws = WebSocketServer()

    asyncio.get_event_loop().run_until_complete(ws.listen())
    asyncio.get_event_loop().run_forever()
