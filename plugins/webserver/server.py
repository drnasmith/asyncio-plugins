# import uvicorn
import time

from hypercorn.asyncio import serve
from hypercorn.config import Config

from fastapi import FastAPI, Request
from starlette.routing import WebSocketRoute
from starlette.middleware import Middleware

from plugins.starlette_websockets.websockets import WebSocketServer

# Custom middleware
from .middleware import GenericMiddleware
from .apps import register_apps

# Splitting up front end from backend allows us to namespace the routes
# So /api routes are mounted under /api
# Frontend needs to go last if its mounted at '/'
# Seems to clash with other mount points if its first
# We can optionally add websocket server to run under the same ASGI app
# Just add it to the main app with a route as follows
# Our main app object
app = FastAPI(
    routes=[
        WebSocketRoute("/ws", WebSocketServer),
        WebSocketRoute("/chat", WebSocketServer),
    ],
    middleware=[Middleware(GenericMiddleware)],
)


# Now attaching the ui/api to the main app object
register_apps(app)


# Example middleware code that intercepts all requests for http
@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    print("Intercepting all calls to app with http protocol")
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response


def hypercorn_runner_task():
    # Hypercorn plays nicely with asyncio
    # The serve method can be passed into an asyncio task list
    config = Config()
    config.bind = ["localhost:8080"]  # As an example configuration setting
    return serve(app, config)


def main():
    hypercorn_runner_task()


if __name__ == "__main__":
    main()
