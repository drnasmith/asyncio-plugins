import importlib


routes = {
    "/api/secure": "secure",
    "/api/auth": "auth",
    "/api/academy-app": "academy",
    "/api": "api",
    "/": "frontend",
}


def load_module(name):
    module_name = f"plugins.webserver.apps.{name}"
    module = importlib.import_module(module_name)
    return module


def register_apps(app):
    for route, module_name in routes.items():
        try:
            module = load_module(module_name)
            app.mount(route, module.app)
        except AttributeError as e:
            print(f"Error loading module {module_name} - {str(e)}")
