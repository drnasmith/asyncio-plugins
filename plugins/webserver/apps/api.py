import asyncio
from typing import Optional
from starlette.middleware import Middleware
from fastapi import FastAPI, Header, HTTPException, Request
from starlette.responses import Response, FileResponse

# For 'secure' file response
# from .responses import SecureFileResponse

# Custom middleware
from plugins.webserver.middleware import CustomHeaderMiddleware

app = FastAPI(middleware=[Middleware(CustomHeaderMiddleware)])

#
# API routes
#
@app.get("/downloads/assets/{assetid}")
async def download():
    try:
        return FileResponse(
            path="static/courses/test.json",
            filename="download-filename.json",
        )
    except:
        return Response(
            status_code=403, content="Permission denied - filemanager says no"
        )


@app.get("/downloads/pdf")
async def download_file():
    try:
        return FileResponse(
            path="static/courses/python_tutorial.pdf",
            filename="download-filename.pdf",
        )
    except:
        return Response(status_code=404, content="Error file not found")


@app.get("/hello")
async def read_hello():
    await asyncio.sleep(0.1)
    return {"Hello": "World"}


@app.get("/items/{item_id}")
async def read_item(item_id: int, q: Optional[str] = None):
    print("Got request")
    await asyncio.sleep(0.1)
    return {"item_id": item_id, "q": q}
