from starlette.responses import FileResponse
from fastapi import FastAPI

app = FastAPI()
#
# Frontend UI routes
#
@app.get("/")
async def read_index():
    return FileResponse("static/index.html")
