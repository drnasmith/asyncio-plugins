import uuid
from fastapi import Depends, FastAPI
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from core.authentication_manager import TheAuthenticationManager
from core.evidence import JWTEvidenceHandler

from core.models import Credentials, UserProfile, UserResponse
from fastapi import Response

from plugins.webserver import SECRET

app = FastAPI()

security = HTTPBasic()


# Example using swagger spec and basic form submission
@app.get("/users/login")
def read_current_user(credentials: HTTPBasicCredentials = Depends(security)):
    return {"username": credentials.username, "password": credentials.password}


@app.post("/login", response_model=UserResponse)
def login(credentials: Credentials):
    authentication_manager = TheAuthenticationManager()
    try:
        result = authentication_manager.authenticate(credentials.dict(), "login")
        print(f"Got result from authentication manager {result}")
        # Only need the jwt handler here to create a token
        jwt = JWTEvidenceHandler(SECRET)
        token = jwt.encode(credentials.dict())

        profile = UserProfile(
            email=f"{credentials.username}@example.com",
            photoURL="https://example.com/me/avatar.png",
            displayName=f"{credentials.username}",
        )
        response = UserResponse(uuid=uuid.uuid4(), profile=profile)
        response.access_token = token
    except RuntimeError:
        response = Response(status_code=401)
    finally:
        return response
