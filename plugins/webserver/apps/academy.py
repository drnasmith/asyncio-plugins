from fastapi import FastAPI
from starlette.middleware import Middleware

import core.fakedb
from plugins.webserver.middleware import CustomHeaderMiddleware

app = FastAPI(middleware=[Middleware(CustomHeaderMiddleware)])


@app.get("/courses")
def get_courses():
    courses = core.fakedb.get_courses()
    return {"courses": courses}


@app.get("/courses/{id}")
def get_courses(id):
    course = core.fakedb.get_course(id)
    return {"course": course}
