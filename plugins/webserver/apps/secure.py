import asyncio
from typing import Optional
from starlette.middleware import Middleware
from fastapi import FastAPI, Header, HTTPException, Request

from plugins.webserver.middleware import JwtTokenMiddleware

app = FastAPI(middleware=[Middleware(JwtTokenMiddleware)])


@app.get("/middleware")
async def middleware(request: Request, authorization: Optional[str] = Header(None)):
    # Making up data for now...
    try:
        user = request.state.user
        print(f"Got user: {user}")
    except KeyError:
        raise HTTPException(status_code=403, detail="Credentials not valid")
    else:
        return {"result": "ok", "username": user}
