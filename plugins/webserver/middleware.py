from starlette.datastructures import URL
from starlette.types import ASGIApp, Receive, Scope, Send
from starlette.middleware.base import BaseHTTPMiddleware
from starlette.responses import Response

from core.authentication_manager import TheAuthenticationManager

# Middleware that works on http and websocket requests
class GenericMiddleware:
    def __init__(self, app: ASGIApp) -> None:
        self.app = app

    async def __call__(self, scope: Scope, receive: Receive, send: Send) -> None:
        if scope["type"] in ("http", "websocket"):
            url = URL(scope=scope)
            print(f"Generic Middleware Running URL: {url}")
        # Not actually doing anything here...
        await self.app(scope, receive, send)


class CustomHeaderMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        print("Custom Middleware Triggered")
        response = await call_next(request)
        response.headers["Custom"] = "Example"
        return response


class JwtTokenMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        # Any request without a token should be rejected
        try:
            authorization_header = request.headers["authorization"]
            token = authorization_header.split(" ")[1]
            user = self.validate_user(token)
            # We can pass extra information into the api via starlette's state property
            request.state.user = user
            response = await call_next(request)
        except KeyError:
            response = Response("No authorization header provided", status_code=401)
        except PermissionError:
            response = Response("Invalid token provided", status_code=403)
        finally:
            return response

    def validate_user(self, token):
        try:
            result = TheAuthenticationManager().authenticate({"token": token}, "jwt")
        except KeyError:
            raise PermissionError("Token not valid")
        except RuntimeError:
            raise PermissionError("Token not valid")
        else:
            return result.get("username")


class BasicAuthMiddleware(BaseHTTPMiddleware):
    async def dispatch(self, request, call_next):
        # At the moment ignore requests without headers...(just testing reading the token)
        if request.headers.get("authorization") is None:
            response = await call_next(request)
            return response
        elif request.headers.get("authorization"):
            fields = request.headers["authorization"].split(" ")
            scheme = fields[0]
            assert scheme.lower() == "basic"
            token = fields[1]
            try:
                user = self.validate_user(token)
                # We can pass extra information into the api via startette's state property
                request.state.user = user
                response = await call_next(request)
            except KeyError:
                print(f"API Middleware invalid user")
                response = Response(status_code=403)
            finally:
                return response

    def validate_user(self, token):
        try:
            result = TheAuthenticationManager().authenticate({"token": token}, "simple")
            return result.get("username")
        except KeyError:
            raise KeyError("Token not valid")
