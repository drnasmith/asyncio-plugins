import sys
from core.authentication_manager import TheAuthenticationManager
from core.evidence import (
    JWTEvidenceHandler,
    SimplePasswordEvidence,
    UserExistsEvidence,
    UserBannedEvidence,
)

SECRET = "123456789"
BANNED_USERS = ["doctor.doom"]


def init():
    jwt = JWTEvidenceHandler(SECRET)
    simple = SimplePasswordEvidence()
    does_user_exist = UserExistsEvidence()
    is_not_banned = UserBannedEvidence(BANNED_USERS)
    TheAuthenticationManager().register_authentication_sequence("login", [simple])
    TheAuthenticationManager().register_authentication_sequence(
        "jwt", [jwt, does_user_exist, is_not_banned]
    )


init()
