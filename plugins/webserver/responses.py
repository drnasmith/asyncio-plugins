"""
Example of extending the file response object with "security"
Principle is we could tap into core functions to check permissions/directories before returning files
"""
import os
import typing

from core.file_manager import FileManager

from starlette.background import BackgroundTask
from starlette.responses import FileResponse


class SecureFileResponse(FileResponse):
    file_manager = FileManager(allowed_filepaths=["static"])

    def __init__(
        self,
        path: typing.Union[str, "os.PathLike[str]"],
        status_code: int = 200,
        headers: dict = None,
        media_type: str = None,
        background: BackgroundTask = None,
        filename: str = None,
        stat_result: os.stat_result = None,
        method: str = None,
    ):
        super().__init__(
            path,
            status_code,
            headers,
            media_type,
            background,
            filename,
            stat_result,
            method,
        )

    async def __call__(self, scope, receive, send):
        if self.file_manager.is_allowed_path(self.path):
            await super().__call__(scope, receive, send)
        else:
            await send(
                {
                    "type": "http.response.start",
                    "status": 403,
                    "headers": self.raw_headers,
                }
            )
            await send(
                {
                    "type": "http.response.body",
                    "body": '{"message": "Error Permission Denied"}',
                }
            )
