from starlette.endpoints import WebSocketEndpoint
from starlette.websockets import WebSocket

# Import list because WebSocket is not hashable (can't use a set)
from typing import List


class WebSocketServer(WebSocketEndpoint):
    encoding = "text"
    connections: List[WebSocket] = []

    async def on_connect(self, websocket):
        await websocket.accept()
        print(f"New Connection established")
        self.connections.append(websocket)

    async def on_disconnect(self, websocket, close_code):
        print(f"Connection Closed with code {close_code}")
        self.connections.remove(websocket)

    async def on_receive(self, websocket, data):
        await self.process_message(data, websocket)

    async def process_message(self, message, websocket):
        print(f"Received message: {message}")
        tokens = message.split(" ")
        if tokens[0] == "HELP":
            reply = f"Valid commands are JOIN <user>, MSG <message string>"
            await self.send_to(reply, [websocket])
        elif tokens[0] == "JOIN":
            reply = f"New User joined: {tokens[1]}"
            clients = [client for client in self.connections if client != websocket]
            await self.send_to(reply, clients)
        elif tokens[0] == "MSG":
            reply = f"MSG: {' '.join(tokens[1:])}"
            await self.send_to_all(reply)
        else:
            reply = f"Message {tokens[0]} not supported"
            await self.send_to(reply, [websocket])

    async def send_to_all(self, message):
        for client in self.connections:
            await client.send_text(message)

    async def send_to(self, message, websockets):
        for websocket in websockets:
            await websocket.send_text(message)
