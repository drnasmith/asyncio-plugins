import asyncio

# import uvicorn
"""
Example of standing up a single websocket server using starlette
"""
from hypercorn.asyncio import serve
from hypercorn.config import Config

from starlette.applications import Starlette
from starlette.routing import WebSocketRoute
from .websockets import WebSocketServer

app = Starlette(
    routes=[
        WebSocketRoute("/ws2", WebSocketServer),
    ]
)


def hypercorn_runner_task():
    # Hypercorn plays nicely with asyncio
    # The serve method can be passed into an asyncio task list
    config = Config()
    config.bind = ["localhost:5000"]  # As an example configuration setting
    return serve(app, config)


def main():
    hypercorn_runner_task()


if __name__ == "__main__":
    main()
