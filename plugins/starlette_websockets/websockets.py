from starlette.endpoints import WebSocketEndpoint
from starlette.websockets import WebSocket

# Import list because WebSocket is not hashable (can't use a set)
from typing import List


class WebSocketServer(WebSocketEndpoint):
    encoding = "text"
    # Note sharing all connections no matter how many instances...
    # Only for testing purposes....
    connections: List[WebSocket] = []

    async def on_connect(self, websocket):
        await websocket.accept()
        print(f"New Connection established on path: {self.scope.get('path')}")
        self.connections.append(websocket)

    async def on_disconnect(self, websocket, close_code):
        print(f"Connection Closed with code {close_code}")
        self.connections.remove(websocket)

    async def on_receive(self, websocket, data):
        await self.process_message(data, websocket)

    async def process_message(self, message, websocket):
        print(f"Received message: {message}")
        print(f"Connection established with scoped user: {self.scope.get('user')}")
        tokens = message.split(" ")
        if tokens[0] == "HELP":
            await self.on_help(websocket)
        elif tokens[0] == "JOIN":
            await self.on_join(websocket, tokens[1])
        elif tokens[0] == "MSG":
            await self.on_msg(websocket, " ".join(tokens[1:]))
        else:
            reply = f"Message {tokens[0]} not supported"
            await self.send_to(reply, [websocket])

    async def on_help(self, websocket):
        reply = f"Valid commands are JOIN <user>, MSG <message string>"
        await self.send_to(reply, [websocket])

    async def on_join(self, websocket, user):
        reply = f"New User joined: {user}"
        clients = [client for client in self.connections if client != websocket]
        await self.send_to(reply, clients)

    async def on_msg(self, websocket, message):
        reply = f"MSG: {message}"
        await self.send_to_all(reply)

    async def send_to_all(self, message):
        for client in self.connections:
            await client.send_text(message)

    async def send_to(self, message, websockets):
        for websocket in websockets:
            await websocket.send_text(message)
