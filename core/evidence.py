import base64
import jwt
from jwt.exceptions import ExpiredSignatureError, InvalidSignatureError, DecodeError
from datetime import datetime, timedelta
import hashlib
from collections import UserDict
from enum import Enum

users = [
    "peter.parker@dailybugle.com:spiderman",
    "boaty@example.com:mcboatface",
]


class AuthenticationState(Enum):
    FAILED = -1
    INITIALIZED = 0
    PARTIAL = 1
    PASSED = 2


class Evidence(UserDict):
    """
    Store the evidence used to authenticate a user
    The initial dictionary is set as read only
    Subsequence steps can add/edit fields as required
    Final result should be updated by a manager class
    """

    def __init__(self, initial_data):
        self._readonly_keys = []
        self._result = AuthenticationState.INITIALIZED
        # Store any failure message here
        self._message = ""
        super().__init__(initial_data)
        # Note set ro after the initial setup
        self._readonly_keys = initial_data.keys()

    def __setitem__(self, key, value):
        if key in self._readonly_keys:
            raise NotImplementedError("This field can not be changed")
        else:
            self.data[key] = value

    def __delitem__(self, key):
        if key in self._readonly_keys:
            raise NotImplementedError("This field can not be changed")
        else:
            del self.data[key]

    def get_result(self):
        return self._result

    def set_result(self, result: AuthenticationState):
        self._result = result

    def set_message(self, message: str):
        self._message = message

    def __str__(self):
        return f"Evidence result: {self._result}, message: {self._message}"

    def __repr__(self):
        return f"Result: {self._result}, message: {self._message}, data: {self.data}"


class EvidenceHandler:
    """
    Process evidence of a certain type
    Generate needed to generate tokens but not all evidence types would need such a method
    Evidence is a user dict which means we can chain evidence objects together
    """

    def verify(self, evidence: Evidence):
        raise NotImplementedError

    def generate(self, parameters: dict):
        raise NotImplementedError


class UserExistsEvidence(EvidenceHandler):
    """
    Simple check to see if provided username exists in our system records...
    """

    def __init__(self, users=users):
        self.users = []
        self._init_users(users)

    def _init_users(self, users):
        for user in users:
            username = user.split(":")[0]
            self.users.append(username)

    def verify(self, evidence):
        try:
            username = evidence.get("username")
            valid_user = username if username in self.users else None
            if not valid_user:
                raise RuntimeError(f"Username {username} is not a valid user")
        except KeyError:
            raise RuntimeError(f"Provided evidence should include username property")

    def generate(self, parameters):
        raise RuntimeError("UserExistsEvidence does not provide an encode method")


class SimplePasswordEvidence(EvidenceHandler):
    """
    Should use passlib or some unique password hash generation in prod
    """

    def __init__(self, users=users):
        self.hashed_users = []
        self._init_users(users)

    def _init_users(self, users):
        for user in users:
            hash = self._generate_hash(user)
            self.hashed_users.append(hash)

    def _generate_hash(self, credentials):
        return hashlib.md5(credentials.encode())

    def verify(self, evidence):
        result = {}
        try:
            username = evidence["username"]
            password = evidence["password"]

            hashed_credentials = self._generate_hash(f"{username}:{password}")

            for user in self.hashed_users:
                if user.digest() == hashed_credentials.digest():
                    result["username"] = user
                    break
        except KeyError:
            raise RuntimeError(
                "Could not get required parameters from credentials evidence: username and password"
            )

        if result.get("username") is None:
            raise RuntimeError(f"Username {username} is not in list of allowed users")

    def generate(self, parameters: dict):
        try:
            username = parameters["username"]
            password = parameters["password"]

            hashed_credentials = self._generate_hash(f"{username}:{password}")
        except KeyError:
            raise RuntimeError(
                "Could not get required parameters from parameters: username and password"
            )
        else:
            return hashed_credentials


class BasicEvidenceHandler(EvidenceHandler):
    """
    Handle base64 encoded username:password strings
    """

    def verify(self, evidence: dict):
        try:
            token = evidence["token"]
            decoded_auth = base64.b64decode(token)
            username, password = decoded_auth.decode().split(":")
            evidence.setdefault("username", username)
            evidence.setdefault("password", password)
        except KeyError:
            raise RuntimeError("Could not get required parameters from evidence: token")

    def generate(self, parameters: dict):
        username = parameters.get("username", "peter")
        password = parameters.get("password", "spiderman")
        credentials = f"{username}:{password}"
        return base64.b64encode(credentials.encode())


class JWTEvidenceHandler(EvidenceHandler):
    def __init__(self, secret, expiry=timedelta(hours=1)):
        self.expiry = expiry
        self.secret = secret
        self.algorithms = ["HS256"]

    def generate(self, parameters):
        expiry = self._generate_expiry()
        username = parameters.get("username")
        data = {"username": username}
        return jwt.encode({"data": data, "exp": expiry}, self.secret)

    def _generate_expiry(self):
        return datetime.utcnow() + self.expiry

    def verify(self, evidence: dict):
        try:
            token = evidence["token"]
            decoded = jwt.decode(token, self.secret, self.algorithms)
            evidence.setdefault("username", decoded["data"]["username"])
        except KeyError:
            raise RuntimeError("Could evidence did not correct parameters: token")
        except DecodeError:
            raise RuntimeError("Could not decode token - invalid format")
        except InvalidSignatureError:
            raise RuntimeError(
                "Signature is invalid for this token - check secret and algorithms are set correctly"
            )
        except ExpiredSignatureError:
            raise RuntimeError("User token is expired, login and try again")


class UserBannedEvidence(EvidenceHandler):
    def __init__(self, banned_list):
        self.banned_list = banned_list

    def verify(self, evidence):
        """
        If a user is on the banned list we should throw an exception
        Returning a results dictionary implies success
        """
        user = evidence["username"]
        if user in self.banned_list:
            raise RuntimeError("")

    def generate(self, parameters):
        return parameters
