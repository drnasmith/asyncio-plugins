import asyncio
from contextlib import suppress
import inspect


class TaskManager:
    __instance = None

    @staticmethod
    def get_instance():
        if TaskManager.__instance:
            return TaskManager.__instance

        TaskManager.__instance = TaskManager()

        return TaskManager.__instance

    def __init__(self):
        if TaskManager.__instance:
            raise AttributeError("Only one task manager can be instantiated")
        self.tasks = set()
        TaskManager.__instance = self

    def register_task(self, task) -> bool:
        """
        The task here needs to be something that is awaitable
        Like a Future, coroutine or asyncio.Task
        """
        if inspect.isawaitable(task) is not True:
            raise TypeError(
                "TaskManager:register_task - passed object was not awaitable (coroutine, task etc.)"
            )

        self.tasks.add(task)

    def register_coroutine(self, callable, *args, **kwargs):
        try:
            # Create task schedules the task to run in the event loop
            task = asyncio.get_event_loop().create_task(callable(*args, **kwargs))
            self.register_task(task)
        except TypeError:
            raise TypeError(
                "TaskManager:register_coroutine - passed function was not a coroutine"
            )

    async def execute_tasks(self):
        """
        This method waits for all tasks/routines to complete
        """
        try:
            await asyncio.gather(*self.tasks)
        except TypeError:
            raise TypeError(
                "TaskManager:execute_tasks - unable to gather tasks, not all tasks are valid awaitable items (coroutines, futures etc.)"
            )

    def _cancel_all_tasks(self):
        # Let's also cancel all running tasks:
        tasks = asyncio.all_tasks()
        for task in tasks:
            print(f"Cancelling task: {task.get_name()}")

            task.cancel()
            with suppress(asyncio.CancelledError):
                print("Caught Task cancel exception - ignore we are cancelling it...")

    def _clear_registered_tasks(self):
        self.tasks = set()

    def stop(self):
        print("TaskManager:stop")
        loop = asyncio.get_event_loop()
        loop.stop()

    def reset_tasks(self):
        """
        If running execute_tasks multiple times we can just keep adding tasks
        Those tasks that have already finished will not run again
        However, cleaner to perform some housekeeping and remove tasks that have finished
        """
        self.tasks = set()

    def run(self):
        try:
            if self.tasks:
                self.register_coroutine(self.execute_tasks)
            asyncio.get_event_loop().run_forever()
        except RuntimeError:
            pass
        except KeyboardInterrupt:
            pass
        finally:
            self.stop()

    def run_until_tasks_complete(self):
        try:
            asyncio.get_event_loop().run_until_complete(self.execute_tasks())
        except RuntimeError:
            print("Caught exception running task list...")
        finally:
            self._clear_registered_tasks()
