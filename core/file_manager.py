import aiofiles
import os


class FileManager:
    def __init__(self, allowed_filepaths=[]):
        """
        Allowed filepaths should be the root prefix /tmp, /public etc.
        Can extend this once more clarity on data storage...
        """
        self.allowed_paths = allowed_filepaths

    def is_allowed_path(self, filename):
        """
        Testing an exact match between an allowed path and the filepath
        """
        target_path = os.path.dirname(filename)
        for path in self.allowed_paths:
            if os.path.commonpath([target_path, path]) == path:
                return True
        return False

    async def read(self, filename, mode="r"):
        if self.is_allowed_path(filename) is False:
            raise PermissionError

        async with aiofiles.open(filename, mode=mode) as f:
            contents = await f.read()
            return contents

    async def read_lines(self, filename, mode="r"):
        if self.is_allowed_path(filename) is False:
            raise PermissionError

        async with aiofiles.open(filename, mode=mode) as f:
            async for line in f:
                yield line

    async def write(self, filename, content, mode="w"):
        if self.is_allowed_path(filename) is False:
            raise PermissionError

        # If file exists, ignore or overwrite?
        async with aiofiles.open(filename, mode=mode) as f:
            await f.write(content)

    async def append(self, filename, content, mode="a"):
        if self.is_allowed_path(filename) is False:
            raise PermissionError

        # If file exists, ignore or overwrite?
        async with aiofiles.open(filename, mode=mode) as f:
            await f.write(content)
