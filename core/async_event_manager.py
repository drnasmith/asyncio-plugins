import asyncio


class AsyncEventManager:
    def __init__(self):
        self.listeners = {}

    def add_event_listener(self, event_name, listener):
        # Using a list to store the listeners so order is preserved
        if event_name not in self.listeners:
            self.listeners[event_name] = list()

        if listener not in self.listeners[event_name]:
            self.listeners[event_name].append(listener)

        return True

    def remove_event_listener(self, event_name, listener):
        listeners = self.listeners.get(event_name)
        try:
            listeners.remove(listener)
        except AttributeError:
            print(f"No event of that type")
        except KeyError:
            print(f"No listener of that type")

    def emit(self, event, payload):
        try:
            results = asyncio.gather(
                *[listener(payload) for listener in self.listeners[event]],
                return_exceptions=True,
            )
            return results
        except KeyError:
            print(f"No event of that name")
            return []
