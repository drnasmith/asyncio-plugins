import uuid
from .models import Course

ids = []

for _ in range(3):
    ids.append(uuid.uuid4())

courses = {
    "1": Course(id=ids[0], title="course 1", version="1.1"),
    "2": Course(id=ids[1], title="course 2", version="1.2"),
    "3": Course(id=ids[2], title="course 3", version="1.3"),
}


def get_courses():
    results = [value for value in courses.values()]
    return results


def get_course(id):
    course = courses.get(str(id))
    return course
