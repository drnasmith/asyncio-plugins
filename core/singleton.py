# Basis singleton pattern
# Works with single inheritance
class Singleton(object):
    _instance = None

    def __new__(class_, *args, **kwargs):
        if not isinstance(class_._instance, class_):
            print(f"Singleton Creating class instance {class_} ...")
            class_._instance = object.__new__(class_, *args, **kwargs)
            print(f"...instance created")
        return class_._instance

    @classmethod
    def get_instance(clss):
        if clss._instance:
            print(f"Class method: {clss} already exists")
            return clss._instance
        else:
            print(f"Creating new instance of {clss}")
            return clss()


class SingletonBase(object):
    _instance = None

    def __new__(class_, *args, **kwargs):
        if not isinstance(class_._instance, class_):
            print(f"Singleton Creating class instance {class_} ...")
            class_._instance = object.__new__(class_, *args, **kwargs)
            print(f"...instance created")
        return class_._instance


# Singleton pattern as described in the python documentation __new__
# https://www.python.org/download/releases/2.2/descrintro/#__new__
# Similar to the above - allows init method to be called on creation
class TheSingleton(object):
    def __new__(cls, *args, **kwds):
        it = cls.__dict__.get("__it__")
        if it is not None:
            return it

        print(f"TheSingleton Creating new instance of {cls}")
        cls.__it__ = it = object.__new__(cls)
        it.init(*args, **kwds)
        return it

    @classmethod
    def instance(cls):
        if cls.__it__:
            print(f"Class method: {cls} already exists")
            return cls.__it__
        else:
            print(f"Creating new instance of {cls}")
            return cls()

    def init(self, *args, **kwds):
        """
        Override this method in the sub classes
        """
        pass


# Can also create pools - Multi-ton(!)
# https://howto.lintel.in/python-__new__-magic-method-explained/
class LimitedInstances(object):
    _instances = []  # Keep track of instance reference
    limit = 5

    def __new__(cls, *args, **kwargs):
        if not len(cls._instances) <= cls.limit:
            raise RuntimeError(f"Count not create instance. Limit {cls.limit} reached")
        instance = object.__new__(cls, *args, **kwargs)
        cls._instances.append(instance)
        return instance

    def __del__(self):
        # Remove instance from _instances
        self._instance.remove(self)


# Metaclass approach
class MetaSingleton(type):
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(MetaSingleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class MetaLogger(metaclass=MetaSingleton):
    def __init__(self, name="logger"):
        self.name = name
        print(f"MetaLogger created with {name}")

    def get_name(self):
        return self.name


class NewLogger(MetaLogger):
    def __init__(self):
        super().__init__("new_logger")
        print("New Logger")


class Logger(SingletonBase):
    messages = []

    def __init__(self, name="logger_name"):
        print(f"Logger init constructor")
        self.name = name

    def add(self, message):
        self.messages.append(message)

    def __str__(self):
        return f"Logger [{self.name}]: {','.join(self.messages)}"


class PrintLogger(Logger):
    def __init__(self):
        super().__init__(name="print_logger")
        print(f"Print Logger init constructor")

    # def __str__(self):
    #     return f"PrintLogger [{self.name}]: {','.join(self.messages)}"


#
# Can either call constructor and always get the same instance
#
def get_logger():
    return Logger()


#
# Or call an instance method on the class - less surprising maybe?
#
def get_print_logger():
    return PrintLogger()


def get_task_manager():
    return TaskManager.get_instance()


class TaskManager(Singleton):
    counter = 0

    def __init__(self):
        print("TaskManager __init__")

    def increment(self):
        self.counter += 1

    def __repr__(self):
        return f"TaskManager: {self.counter}"


def test_logger():
    logger1 = Logger()
    maybe_print = PrintLogger()
    logger2 = PrintLogger()

    assert maybe_print is not logger1
    logger1.add("Hello")
    logger2.add("World")

    maybe_print.add("PRINT LOGGER")

    print(logger2)

    logger = get_logger()
    print_logger = get_print_logger()

    print(logger)
    print(print_logger)

    for _ in range(30):
        PrintLogger()


def test_task_manager():
    tm2 = get_task_manager()
    tm3 = get_task_manager()
    tm0 = TaskManager()
    tm1 = TaskManager()

    assert tm0 == tm3
    tm1.increment()
    tm1.increment()
    print(tm1)
    tm = get_task_manager()
    print(tm)


def test_meta_singleton():
    m1 = MetaLogger("first_try")
    m2 = MetaLogger("something-else")
    n1 = NewLogger()

    for _ in range(30):
        MetaLogger()
    assert m1 == m2
    assert n1 is not m1

    print(f"MetaLogger m1 is called {m1.get_name()}")
    print(f"MetaLogger m2 is called {m2.get_name()}")
    print(f"New Logger is called {n1.get_name()}")


if __name__ == "__main__":
    # test_task_manager()
    test_logger()
    # test_meta_singleton()
