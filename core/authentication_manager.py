from core.singleton import MetaSingleton
from core.evidence import AuthenticationState

# Non singleton version - see below for singleton implementation...
class AuthenticationManager:
    """
    Authentication Manager stores a set of sequences
    In essence these are workflows - a series of steps that must be passed in order to authenticate
    Each step in the process produces a result which in turn is passed into the next evidence handler
    So the order matters. An example would be decode-token => check username and password => check user not banned/allowed etc.
    """

    def __init__(self):
        self._authentication_sequence = {}

    def register_authentication_sequence(self, name, handlers):
        self._authentication_sequence[name] = handlers

    def authenticate(self, evidence, handler_name):
        try:
            handlers = self._authentication_sequence[handler_name]
            for handler in handlers:
                handler.verify(evidence)
                evidence.set_result(AuthenticationState.PARTIAL)
            evidence.set_result(AuthenticationState.PASSED)
        except KeyError:
            evidence.set_result(AuthenticationState.FAILED)
            evidence.set_message(f"Authentication process {handler_name} failed")
            raise RuntimeError(
                f"No handler for this type of evidence found {handler_name}"
            )
        except RuntimeError as e:
            evidence.set_result(AuthenticationState.FAILED)
            evidence.set_message(f"Authentication process {handler_name} failed")
            raise RuntimeError(f"Authentication sequence failed - {str(e)}")
        return evidence


class TheAuthenticationManager(metaclass=MetaSingleton):
    """
    Authentication Manager stores a set of sequences
    In essence these are workflows - a series of steps that must be passed in order to authenticate
    Each step in the process produces a result which in turn is passed into the next evidence handler
    So the order matters. An example would be decode-token => check username and password => check user not banned/allowed etc.
    """

    _authentication_sequence = {}

    def register_authentication_sequence(self, name, handlers):
        self._authentication_sequence[name] = handlers

    def authenticate(self, evidence, handler_name):
        try:
            handlers = self._authentication_sequence[handler_name]
            for handler in handlers:
                handler.verify(evidence)
            evidence.set_result(AuthenticationState.PASSED)
        except KeyError:
            evidence.set_result(AuthenticationState.FAILED)
            evidence.set_message(f"Authentication process {handler_name} failed")
            raise RuntimeError(
                f"No handler for this type of evidence found {handler_name}"
            )
        except RuntimeError as e:
            evidence.set_result(AuthenticationState.FAILED)
            evidence.set_message(f"Authentication process {handler_name} failed")
            raise RuntimeError(f"Authentication sequence failed - {str(e)}")
        return evidence


#
# Alternatively we could build an authentication workflow and register that in one go
#
class AuthenticationStep:
    """
    Each step can be one or more evidence handlers
    The default is that 'all' handlers must pass for this step to pass (AND)
    Override the rule to set 'any' so only one handler needs to pass (OR)
    """

    ...

    def __init__(self, handlers, rule="all"):
        if rule in ["all", "any"]:
            self._rule = rule
            self._handlers = handlers
        else:
            raise KeyError("Only all or any rules are allowed")

    def verify(self, evidence):
        results = []
        for handler in self._handlers:
            try:
                handler.verify(evidence)
                results.append(True)
            except RuntimeError:
                results.append(False)
        if self._rule == "all":
            consolidated_result = all(results)
        else:
            consolidated_result = any(results)
        assert consolidated_result


class AuthenticationWorkflow:
    def __init__(self):
        self._steps = []

    def add_authentication_step(self, handler):
        self._steps.append(handler)

    def authenticate(self, evidence):
        try:
            for step in self._steps:
                step.verify(evidence)
                evidence.set_result(AuthenticationState.PARTIAL)
        except AssertionError as e:
            evidence.set_result(AuthenticationState.FAILED)
            evidence.set_message(str(e))
            raise RuntimeError(f"Error processing authentication workflow - {str(e)}")
        except RuntimeError as e:
            evidence.set_result(AuthenticationState.FAILED)
            evidence.set_message(str(e))
            raise RuntimeError(f"Error processing authentication workflow - {str(e)}")
        else:
            evidence.set_result(AuthenticationState.PASSED)
            return evidence
