from pydantic import BaseModel
from typing import List, Optional
from uuid import UUID


class PxtEntity(BaseModel):
    pass


class Credentials(PxtEntity):
    username: str
    password: str


class CourseContent(PxtEntity):
    id: Optional[UUID] = None
    title: Optional[str] = None
    content: Optional[str] = None


class Course(PxtEntity):
    id: UUID
    key: str = ""
    title: str
    description: Optional[str] = None
    labelType: Optional[str] = None
    hours: Optional[int] = None
    totalSteps: int = 0
    activeSteps: int = 1
    steps: List[CourseContent] = []
    version: str = ""


class CourseResponse(PxtEntity):
    course: Course
    attachments: List[str] = []


class Courses(PxtEntity):
    courses: Optional[List[Course]] = None


class UserProfile(PxtEntity):
    displayName: str = ""
    photoURL: str = ""
    email: str = ""


class User(PxtEntity):
    uuid: UUID
    first_name: str
    last_name: str
    email: str = ""
    branch: str = ""


class UserResponse(PxtEntity):
    uuid: UUID
    role: dict = {}
    access_token: str = ""
    profile: UserProfile
