class EventManager:
    def __init__(self):
        self.listeners = {}

    def add_event_listener(self, event_name, listener):
        if event_name not in self.listeners:
            self.listeners[event_name] = set()

        self.listeners[event_name].add(listener)

    def remove_event_listener(self, event_name, listener):
        listeners = self.listeners.get(event_name)
        try:
            listeners.remove(listener)
        except AttributeError:
            print(f"No event of that type")
        except KeyError:
            print(f"No listener of that type")

    def emit(self, event, payload):
        try:
            for listener in self.listeners[event]:
                print(f"Sending payload: {payload}")
                listener(payload)
            return True
        except KeyError:
            print(f"No event of that name")
            return False
