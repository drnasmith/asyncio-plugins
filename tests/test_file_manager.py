import asyncio
import json
import tempfile
import os
import platform
import pytest

from core.file_manager import FileManager

test_json_data = """
{
    "key1": "value1",
    "key2": "value2",
    "key3": "value3"
}
"""

ALLOWED_FILEPATH = tempfile.gettempdir()
TEST_JSON_FILE = os.path.join(ALLOWED_FILEPATH, "test_filename.json")
TEST_CSV_FILE = os.path.join(ALLOWED_FILEPATH, "test_filename.csv")

# Using commonpath with file manager will not match anything if drive is not present on windows
DISALLOWED_FILEPATH = (
    "C:\\not\\allowed" if platform.system() == "Windows" else "/not/allowed"
)

TEST_INVALID_FILE = os.path.join(DISALLOWED_FILEPATH, "test_filename.json")


@pytest.fixture()
def _create_temp_json_file():
    filename = TEST_JSON_FILE
    with open(filename, "w") as f:
        f.write(test_json_data)


async def read_file():
    filename = TEST_JSON_FILE
    file_manager = FileManager(allowed_filepaths=[ALLOWED_FILEPATH])
    content = await file_manager.read(filename)
    data = json.loads(content)
    assert data["key1"] == "value1"
    assert data["key2"] == "value2"
    assert data["key3"] == "value3"


async def read_file_by_line():
    filename = TEST_JSON_FILE
    file_manager = FileManager(allowed_filepaths=[ALLOWED_FILEPATH])
    count = 0
    async for line in file_manager.read_lines(filename):
        count += 1
        print(line)
        assert line
    assert count == 5


async def invalid_read_file():
    filename = TEST_INVALID_FILE
    file_manager = FileManager(allowed_filepaths=[ALLOWED_FILEPATH])
    with pytest.raises(PermissionError):
        content = await file_manager.read(filename)


async def write_file():
    filename = TEST_CSV_FILE
    file_manager = FileManager(allowed_filepaths=[ALLOWED_FILEPATH])
    data = ["user=Alice", "email=alice@example.com"]
    content = ",".join(data)
    await file_manager.write(filename, content)


async def append_file():
    filename = TEST_CSV_FILE
    file_manager = FileManager(allowed_filepaths=[ALLOWED_FILEPATH])
    data = ["user=Joe", "email=jbloggs@example.com"]
    content = "\n"
    content += ",".join(data)
    await file_manager.append(filename, content)


def test_async_file_manager_read(_create_temp_json_file):
    run_async_test(read_file)


def test_async_file_manager_read_async(_create_temp_json_file):
    run_async_test(read_file_by_line)


def test_async_file_manager_invalid_read(_create_temp_json_file):
    run_async_test(invalid_read_file)


def test_async_file_manager_write():
    run_async_test(write_file)


def test_async_file_manager_read_async():
    run_async_test(append_file)


def run_async_test(callable):
    loop = asyncio.new_event_loop()
    try:
        loop.run_until_complete(callable())
    finally:
        # Note https://docs.python.org/3/library/asyncio-eventloop.html#asyncio.loop.shutdown_asyncgens
        loop.run_until_complete(loop.shutdown_asyncgens())
        loop.close()
