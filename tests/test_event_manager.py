from core.event_manager import EventManager


def listener1(payload):
    print(f"listenerr 1 event {payload}")
    assert payload.get("value") == 42


def listener2(payload):
    print(f"listenerr 2 event {payload}")
    assert payload.get("value") == 42


def listener_assert_false(payload):
    """This function should be added, then removed otherwise it will fail the test"""
    print(f"listenerr 2 event {payload}")
    assert False


def listener_assert_true(payload):
    """This function should be added"""
    print(f"listenerr event {payload}")
    assert True


def test_listener_event():
    em = EventManager()
    em.add_event_listener("myevent", listener1)
    em.add_event_listener("myevent", listener2)
    payload = {"value": 42}
    em.emit("myevent", payload)


def test_remove_event():
    em = EventManager()
    # Test remove an event that has not been registered yet
    em.remove_event_listener("myevent", listener_assert_false)
    em.add_event_listener("myevent", listener_assert_true)
    em.remove_event_listener("myevent", listener_assert_false)

    em.add_event_listener("myevent", listener_assert_false)
    em.remove_event_listener("myevent", listener_assert_false)

    payload = {"message": "Should never be received"}
    em.emit("myevent", payload)

    em.emit("non_existent_event", payload)
