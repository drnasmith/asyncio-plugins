import pytest
from core.authentication_manager import (
    AuthenticationManager,
    AuthenticationStep,
    AuthenticationWorkflow,
)
from core.evidence import (
    AuthenticationState,
    JWTEvidenceHandler,
    BasicEvidenceHandler,
    SimplePasswordEvidence,
    UserBannedEvidence,
    Evidence,
)


@pytest.fixture
def user():
    return {"username": "peter.parker@dailybugle.com", "password": "spiderman"}


@pytest.fixture
def invalid_user():
    return {"username": "doctor.doom@evil.com", "password": "doesnotexist"}


def test_jwt_authentication(user):
    jwt_evidence = JWTEvidenceHandler(secret="12345")

    # Just setting up the test - simulate getting a token from outside
    token = jwt_evidence.generate(user)

    authentication_manager = AuthenticationManager()
    authentication_manager.register_authentication_sequence("jwt", [jwt_evidence])

    evidence = Evidence({"token": token})

    result = authentication_manager.authenticate(evidence, "jwt")

    assert result["username"] == user["username"]
    assert evidence.get_result() == AuthenticationState.PASSED


def test_basic_authentication(user):
    basic_evidence = BasicEvidenceHandler()

    token = basic_evidence.generate(user)

    authentication_manager = AuthenticationManager()
    authentication_manager.register_authentication_sequence("basic", [basic_evidence])

    evidence = Evidence({"token": token})

    result = authentication_manager.authenticate(evidence, "basic")

    assert result["username"] == user["username"]
    assert evidence.get_result() == AuthenticationState.PASSED


def test_login_authentication(user):
    """
    If logging in for first time, pass username password
    """
    password_evidence = SimplePasswordEvidence()

    authentication_manager = AuthenticationManager()
    authentication_manager.register_authentication_sequence(
        "login", [password_evidence]
    )

    credentials = Evidence(user)

    result = authentication_manager.authenticate(credentials, "login")
    assert result["username"] == user["username"]
    assert credentials.get_result() == AuthenticationState.PASSED

    # so the user is valid - now we could generate a jwt token for subsequent login methods


def test_multipart_authentication(user):
    basic_evidence = BasicEvidenceHandler()
    password_evidence = SimplePasswordEvidence()

    token = basic_evidence.generate(user)

    authentication_manager = AuthenticationManager()
    authentication_manager.register_authentication_sequence(
        "multi", [basic_evidence, password_evidence]
    )

    evidence = Evidence({"token": token})

    result = authentication_manager.authenticate(evidence, "multi")

    assert result["username"] == user["username"]


def test_multipart_authentication_invalid_user(invalid_user):
    basic_evidence = BasicEvidenceHandler()
    password_evidence = SimplePasswordEvidence()

    token = basic_evidence.generate(invalid_user)

    authentication_manager = AuthenticationManager()
    authentication_manager.register_authentication_sequence(
        "multi", [basic_evidence, password_evidence]
    )

    evidence = Evidence({"token": token})

    with pytest.raises(RuntimeError):
        _ = authentication_manager.authenticate(evidence, "multi")


def test_multipart_authentication_invalid_sequence(user):
    basic_evidence = BasicEvidenceHandler()
    password_evidence = SimplePasswordEvidence()

    token = basic_evidence.generate(user)

    authentication_manager = AuthenticationManager()
    authentication_manager.register_authentication_sequence(
        "multi", [basic_evidence, password_evidence]
    )

    evidence = Evidence({"token": token})

    with pytest.raises(RuntimeError):
        _ = authentication_manager.authenticate(evidence, "invalid_sequence")


def test_authentication_workflow(user, invalid_user):
    basic_evidence = BasicEvidenceHandler()
    banned_fake_user = UserBannedEvidence(invalid_user["username"])
    banned_real_user = UserBannedEvidence(user["username"])
    password_evidence = SimplePasswordEvidence()

    token = basic_evidence.generate(user)

    # Workflow has option for AND (all) and OR (any) success criteria
    step1 = AuthenticationStep([basic_evidence])
    step2 = AuthenticationStep([banned_fake_user, password_evidence])
    step3_fail = AuthenticationStep([banned_real_user, password_evidence], rule="all")
    step3_pass = AuthenticationStep([banned_real_user, password_evidence], rule="any")

    #
    # Simple two step process should be OK
    #
    workflow = AuthenticationWorkflow()
    workflow.add_authentication_step(step1)
    workflow.add_authentication_step(step2)

    evidence = Evidence({"token": token})

    _ = workflow.authenticate(evidence)
    assert evidence.get_result() == AuthenticationState.PASSED

    #
    # Force a failure by making second step fail
    #
    workflow = AuthenticationWorkflow()
    workflow.add_authentication_step(step1)
    workflow.add_authentication_step(step3_fail)

    evidence = Evidence({"token": token})

    with pytest.raises(RuntimeError):
        _ = workflow.authenticate(evidence)
    assert evidence.get_result() == AuthenticationState.FAILED

    #
    # A step with an OR condition should be ok
    #
    workflow = AuthenticationWorkflow()
    workflow.add_authentication_step(step1)
    workflow.add_authentication_step(step3_pass)

    evidence = Evidence({"token": token})

    _ = workflow.authenticate(evidence)
    assert evidence.get_result() == AuthenticationState.PASSED
