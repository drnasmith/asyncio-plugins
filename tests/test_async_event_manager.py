import asyncio
from core.async_event_manager import AsyncEventManager


async def listener_return_true(payload):
    print(f"listener_return_true {payload} TRUE")
    await asyncio.sleep(0.1)
    assert payload.get("value") == 42
    return True


async def listener_return_false(payload):
    print(f"listener_return_false {payload} FALSE")
    await asyncio.sleep(0.3)
    assert payload.get("value") == 42
    return False


async def listener_assert_false(payload):
    """This function should be added, then removed otherwise it will fail the test"""
    print(f"listener_assert_false {payload}")
    await asyncio.sleep(0.1)
    assert False


async def listener_assert_true(payload):
    """This function should be added"""
    await asyncio.sleep(0.1)
    print(f"listener event {payload}")
    assert True


async def execute_add_event():
    em = AsyncEventManager()
    em.add_event_listener("myevent", listener_return_true)
    em.add_event_listener("myevent", listener_return_false)
    payload = {"value": 42}
    results = await em.emit("myevent", payload)
    print(f"Results of execute add event = {results}")
    assert results == [True, False]


async def execute_remove_event():
    em = AsyncEventManager()
    # Test remove an event that has not been registered yet
    em.remove_event_listener("myevent", listener_assert_false)
    em.add_event_listener("myevent", listener_assert_true)
    em.remove_event_listener("myevent", listener_assert_false)

    em.add_event_listener("myevent", listener_assert_false)
    em.remove_event_listener("myevent", listener_assert_false)

    payload = {"message": "Should never be received"}

    await em.emit("myevent", payload)


def test_add_event():
    asyncio.get_event_loop().run_until_complete(execute_add_event())


# def test_remove_event():
#     asyncio.get_event_loop().run_until_complete(execute_remove_event())
