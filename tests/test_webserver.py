import pytest
from fastapi.testclient import TestClient

from plugins.webserver.server import app

client = TestClient(app)


@pytest.fixture
def valid_user():
    return {"username": "peter.parker@dailybugle.com", "password": "spiderman"}


@pytest.fixture
def invalid_username():
    return {"username": "peter.parker@nothere.com", "password": "spiderman"}


@pytest.fixture
def invalid_password():
    return {"username": "peter.parker@dailybugle.com", "password": "ironman sucks"}


def test_rest_read_main():
    response = client.get("/")
    assert response.status_code == 200


def test_rest_login_ok(valid_user):
    response = client.post(
        "/api/auth/login",
        json=valid_user,
    )
    assert response.status_code == 200


def test_rest_login_invalid_password(invalid_password):
    response = client.post(
        "/api/auth/login",
        json=invalid_password,
    )
    assert response.status_code == 401


def test_rest_login_invalid_user(invalid_username):
    response = client.post(
        "/api/auth/login",
        json=invalid_username,
    )
    assert response.status_code == 401


def test_rest_get_hello():
    response = client.get("/api/hello")
    assert response.status_code == 200
    assert response.json() == {"Hello": "World"}


def test_rest_get_notfound():
    response = client.get("/api/doesnotexist")
    assert response.status_code == 404


def test_rest_authorization_token(valid_user):
    response = client.post("/api/auth/login", json=valid_user)
    assert response.status_code == 200

    print(response.json())
    token = response.json().get("access_token")
    secure_response = client.get(
        "/api/secure", headers={"authorization": f"Bearer {token}"}
    )

    assert secure_response.status_code == 200


def test_rest_authorization_token(valid_user):
    response = client.post("/api/auth/login", json=valid_user)
    assert response.status_code == 200

    # First try with the token in header
    token = response.json().get("access_token")
    secure_response = client.get(
        "/api/secure/middleware", headers={"authorization": f"Bearer {token}"}
    )
    assert secure_response.status_code == 200
    assert secure_response.json()["result"] == "ok"
    assert secure_response.json()["username"] == valid_user.get("username")


def test_rest_authorization_invalid_token():
    # Same thing but this time without - make sure we get the correct error code
    secure_response = client.get(
        "/api/secure/middleware", headers={"authorization": "Bearer NOT-A-VALID-TOKEN"}
    )
    assert secure_response.status_code == 403

    # And now without any header at all
    secure_response = client.get("/api/secure/middleware")
    assert secure_response.status_code == 401
