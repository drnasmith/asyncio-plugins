from datetime import timedelta
import pytest
from core.evidence import BasicEvidenceHandler, JWTEvidenceHandler, UserBannedEvidence
from core.evidence import AuthenticationState, Evidence


@pytest.fixture
def user():
    return {"username": "peter.parker", "password": "spiderman"}


@pytest.fixture
def banned_user():
    return {"username": "doctor.doom", "password": "spiderman sucks"}


def test_basic_evidence(user):
    handler = BasicEvidenceHandler()
    encoded_data = handler.generate(user)
    evidence = Evidence({"token": encoded_data})

    handler.verify(evidence)
    assert evidence["username"] == user["username"]
    assert evidence["password"] == user["password"]


def test_jwt_evidence(user):
    jwt = JWTEvidenceHandler("1234567890")
    token = jwt.generate(user)
    evidence = Evidence({"token": token})

    jwt.verify(evidence)
    assert evidence["username"] == user["username"]


def test_jwt_invalid_secret(user):
    jwt = JWTEvidenceHandler("1234567890", expiry=timedelta(seconds=1))
    token = jwt.generate(user)

    evidence = Evidence({"token": token})
    jwt = JWTEvidenceHandler("0987654321", expiry=timedelta(seconds=1))
    with pytest.raises(RuntimeError):
        _ = jwt.verify(evidence)


def test_jwt_evidence_expiry(user):
    """
    Deliberately setting an expiry time in the past
    """
    jwt = JWTEvidenceHandler("1234567890", expiry=timedelta(hours=-1))
    token = jwt.generate(user)

    with pytest.raises(RuntimeError):
        _ = jwt.verify({"token": token})


def test_user_banned_evidence(user, banned_user):
    handler = UserBannedEvidence(banned_user["username"])
    evidence = Evidence({"username": user["username"]})

    handler.verify(evidence)
    assert evidence["username"] == user["username"]

    evidence = Evidence({"username": banned_user["username"]})
    with pytest.raises(RuntimeError):
        _ = handler.verify(evidence)


def test_jwt_evidence_invalid_token_format(user):
    jwt = JWTEvidenceHandler("1234567890")
    evidence = Evidence({"token": "NOT-A-VALID-TOKEN"})
    with pytest.raises(RuntimeError):
        _ = jwt.verify(evidence)


def test_evidence_state(user):
    initial_data = {"token": "123456"}
    e = Evidence(initial_data)

    assert e.get_result() == AuthenticationState.INITIALIZED

    e["username"] = user["username"]

    e.set_result(AuthenticationState.PARTIAL)
    assert e.get_result() == AuthenticationState.PARTIAL
