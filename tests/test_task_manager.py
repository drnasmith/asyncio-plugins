import asyncio
import pytest
from core.task_manager import TaskManager


async def valid_coroutine():
    await asyncio.sleep(0)


async def valid_coroutine_with_args(interval, a=1, b=2):
    # Prove that we can call and pass arguments that satisfy this assertion
    await asyncio.sleep(interval)
    assert a == b


def invalid_coroutine():
    pass


async def shutdown_task_manager():
    task_manager = TaskManager.get_instance()
    await asyncio.sleep(1)
    task_manager.stop()


def create_task():
    # This is effectively what the task manager register coroutine method does
    # However, working with other libraries means we may already have a task/awaitable object
    return asyncio.get_event_loop().create_task(valid_coroutine())


async def counter_message():
    count = 5
    while count > 0:
        print(f"Counter Message: {count}")
        await asyncio.sleep(1)
        count = count - 1


async def counter():
    task_manager = TaskManager.get_instance()
    count = 5
    while count > 0:
        print(f"Counter: {count}")
        await asyncio.sleep(1)
        if count == 3:
            task_manager.register_coroutine(counter_message)
        count = count - 1


def test_task_manager_instances():
    t1 = TaskManager.get_instance()
    t2 = TaskManager.get_instance()

    assert t1 == t2

    with pytest.raises(Exception):
        t3 = TaskManager()


def test_task_manager_registrations():
    task_manager = TaskManager.get_instance()
    task_manager.register_coroutine(valid_coroutine)
    task_manager.register_task(create_task())

    with pytest.raises(TypeError):
        task_manager.register_coroutine(None)
        task_manager.register_task(None)
    with pytest.raises(TypeError):
        task_manager.register_coroutine(invalid_coroutine)
    with pytest.raises(TypeError):
        task_manager.register_task(invalid_coroutine())

    task_manager.run_until_tasks_complete()


def test_task_manager_coroutine_with_arguments():
    task_manager = TaskManager.get_instance()
    interval = 0
    task_manager.register_coroutine(valid_coroutine_with_args, interval, b=1)

    task_manager.run_until_tasks_complete()


def test_task_manager_shutdown():
    task_manager = TaskManager.get_instance()

    task_manager.register_coroutine(valid_coroutine_with_args, 5, b=1)
    task_manager.register_coroutine(shutdown_task_manager)

    task_manager.run()


def test_task_manager_run_multiple_times():
    task_manager = TaskManager.get_instance()
    task_manager.register_coroutine(valid_coroutine)
    task_manager.run_until_tasks_complete()
    # We can schedule tasks to run multiple times
    # Each register coroutine creates a new instance of the task
    task_manager.register_coroutine(valid_coroutine)
    task_manager.run_until_tasks_complete()


def test_task_manager_schedule_late_task():
    task_manager = TaskManager.get_instance()
    task_manager.register_coroutine(counter)
    task_manager.run_until_tasks_complete()
