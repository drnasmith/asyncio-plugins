import asyncio
import websockets
from plugins.websockets.server import WebSocketServer

host = "127.0.0.1"
port = 8080


def test_create_websocket_server():
    global host, port
    ws = WebSocketServer(host, port)

    assert ws.host == host
    assert ws.port == port


async def client_connection():
    global host, port
    uri = f"ws://{host}:{port}"
    async with websockets.connect(uri) as websocket:
        message = "HELP"
        await websocket.send(message)
        return_message = await websocket.recv()


async def client_connection_disconnect():
    global host, port
    uri = f"ws://{host}:{port}"
    async with websockets.connect(uri) as websocket:
        pass


def shutdown_event_loop():
    asyncio.get_event_loop().stop()
    # Seemed more reliable to also cancel all running tasks:
    pending = asyncio.all_tasks()
    print(f"Cancelling {len(pending)} outstanding tasks")
    for task in pending:
        task.cancel()


async def run_websocket_connection():
    ws = WebSocketServer("localhost", 8080)

    server_task = ws.listen()
    client_task = asyncio.create_task(client_connection())

    await asyncio.gather(server_task, client_task)


async def run_websocket_disconnection():
    ws = WebSocketServer("localhost", 8080)

    server_task = ws.listen()
    client_task = asyncio.create_task(client_connection_disconnect())

    await asyncio.gather(server_task, client_task)


def test_client_connects_and_disconnects():
    asyncio.run(run_websocket_disconnection())


def test_client_connects():
    asyncio.run(run_websocket_connection())
