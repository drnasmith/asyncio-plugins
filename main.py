import asyncio
from asyncio.tasks import Task
from core.authentication_manager import TheAuthenticationManager
from core.evidence import JWTEvidenceHandler, SimplePasswordEvidence
from tests.test_task_manager import shutdown_task_manager

from core.task_manager import TaskManager

# from plugins.websockets.server import websocket_task
from plugins.webserver.server import hypercorn_runner_task
from plugins.starlette_websockets.server import (
    hypercorn_runner_task as websocket_runner,
)


async def shutdown():
    await asyncio.sleep(5)
    print("Shutting down task manager from coroutine")
    task_manager = TaskManager.get_instance()
    task_manager.stop()


def main():
    task_manager = TaskManager.get_instance()
    # task_manager.register_coroutine(websocket_task())
    # task_manager.register_coroutine(shutdown)
    task_manager.register_task(hypercorn_runner_task())
    # task_manager.register_task(websocket_runner())
    try:
        # task_manager.run_until_tasks_complete()
        task_manager.run()
    except KeyboardInterrupt:
        task_manager.stop()


if __name__ == "__main__":
    print("Main App Running Plugins....")

    # task_manager.run()
    main()
