# asyncio-plugins

Test project to explore asyncio and plugins

## Install
pip install -r requirements.txt

## Testing
Some basic tests are stored in tests folder

## Running
- Run python main.py from the root folder
- This runs a websocket server on port 8080 and a web server on port 5000
- The websocket interface can be tested by running python -m websockets ws://localhost:8080
- Send a message and it will be returned
- Open a web browser to localhost:5000 and you should see a Hello: World JSON response
- Also a url like localhost:5000/items/42?q=searchString should respond with the number 42 and searchString message